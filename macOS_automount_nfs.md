I have spent quite a bit of time figuring out automounts of NFS shares in OS X...

Somewhere along the line, Apple decided allowing mounts directly into /Volumes should not be possible:

/etc/auto_master **(see last line)**:

    #
    # Automounter master map
    #
    +auto_master		# Use directory service
    /net			-hosts		-nobrowse,hidefromfinder,nosuid
    /home			auto_home	-nobrowse,hidefromfinder
    /Network/Servers	-fstab
    /-			-static
    /-			auto_nfs	-nobrowse,nosuid

/etc/auto_nfs **(this is all one line)**:   

    /Volumes/my_mount    -fstype=nfs,noowners,nolockd,noresvport,hard,bg,intr,rw,tcp,nfc nfs://192.168.1.1:/exports/my_share
		
Make sure you:

    sudo chmod 644 /etc/auto_nfs

Otherwise the automounter will not be able to read the config and fail with a  `... parse_entry: getmapent for map failed...` error in `/var/log/messages`

This will not work (anymore!) though it "should".

    $ sudo automount -cv
    ...
    automount: /Volumes/my_mount: mountpoint unavailable
    

**Note that, if you manually create the mount point using `mkdir`, it will mount.
But, upon restart, OS X removes the mount point, and automounting will fail.**


## What's the solution? 
It's so easy my jaw dropped when I figured it out.
Basically, we trick OS X into thinking we're mounting **somewhere else.**

When you're talking about paths in just about any environment, the root folder is the highest path you can reach, whether it's `C:\` (windows) or `/` (*nix)

When you're at this path, attempting to reach the parent path, via `..` will keep you at the root path.

For example: `/../../../../` is still just `/`

By now, a few of you have already figured it out.

## TL;DR / Solution:

Change your `/etc/auto_nfs` config from **(this is all one line)**:

    /Volumes/my_mount    -fstype=nfs,noowners,nolockd,noresvport,hard,bg,intr,rw,tcp,nfc nfs://192.168.1.1:/exports/my_share

For pre-Catalina: To **(this is all one line)**

    /../Volumes/my_mount    -fstype=nfs,noowners,nolockd,noresvport,hard,bg,intr,rw,tcp,nfc nfs://192.168.1.1:/exports/my_share

For Catalina and up: To **(this is all one line)**

    /System/Volumes/Data/../Data/Volumes/my_mount    -fstype=nfs,noowners,nolockd,noresvport,hard,bg,intr,rw,tcp,nfc nfs://192.168.1.1:/exports/my_share


And re-run the automounter:


    $ sudo automount -cv
    ...
    automount: /Volumes/my_mount: mounted
    
..... there you go! Technically `/../Volumes` is still `/Volumes`, but the automounter does not see things that way ;)

This configuration persists the mount across restarts, and creates the mountpoint automatically.